import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Recaptcha from 'react-recaptcha';

const Container = styled.div``;
const Message = styled.span``;

class RecaptchaComponent extends Component {
  constructor() {
    super();
    this.state = {

    };

    this.recaptchaInstance = null;

    this.onVerifyCaptcha = this.onVerifyCaptcha.bind(this);
  }

  onVerifyCaptcha(response) {
    // note: apparently, there's no need to reset the recaptcha
    // this.recaptchaInstance.reset();
    this.props.onVerify(response);
  }

  render() {
    return (
      <Container>
        <Message> To create the short URL, prove you are not a robot: </Message>
        <Recaptcha
          ref={(e) => { this.recaptchaInstance = e; }}
          sitekey="6LfrqVIUAAAAAEe4CmO2YcvVkl5V8VyNVOgHq5ip" // site key is not a secret
          verifyCallback={this.onVerifyCaptcha}
        />
      </Container>
    );
  }
}

RecaptchaComponent.propTypes = {
  onVerify: PropTypes.func.isRequired,
};

export default RecaptchaComponent;
