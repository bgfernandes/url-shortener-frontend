import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import Validations from './../Validations';

const Container = styled.div``;
const ShortUrlInput = styled.input``;
const CheckAvailableButton = styled.button``;
const AvailableUrl = styled.span`
  color: green;
`;
const UnavailableUrl = styled.span`
  color: red;
`;
const ErrorMessage = styled.span`
  color: red;
`;

class ShortUrlComponent extends Component {
  constructor() {
    super();
    this.state = {
      // holds true or false if there was a check to see if the short url is available
      availableResponse: null,

      // holds true if already checking if the short url is available
      availableResponseLoading: false,

      // if there is an error message to be shown, it will be here
      errorMessage: null,

      shortUrlInput: '',
    };

    this.onChangeShortUrl = this.onChangeShortUrl.bind(this);
    this.checkIfShortUrlIsAvailable = this.checkIfShortUrlIsAvailable.bind(this);
  }

  checkIfShortUrlIsAvailable() {
    // if it is already checking, do nothing
    const { availableResponseLoading, shortUrlInput } = this.state;
    if (availableResponseLoading) {
      return;
    }

    // check if shortUrl is valid
    if (!Validations.shortUrlValid(shortUrlInput)) {
      this.setState({
        errorMessage: 'Short URL should have only numbers and letters.',
      });
      return;
    }

    this.setState({
      availableResponse: null,
      availableResponseLoading: true,
    });

    // note: for simplicity, I intend to host this with the backend,
    // but in my development environment I have the server running in a different port
    let url = `/urls/${shortUrlInput}`;
    if (process.env.NODE_ENV === 'development') {
      url = `http://localhost:3000${url}`;
    }

    fetch(url, { redirect: 'manual' }).then((res) => {
      let available;
      if (res.status === 404) {
        available = true;
      } else {
        available = false;
      }

      this.setState({
        availableResponse: available,
        availableResponseLoading: false,
      });
    });
  }

  onChangeShortUrl(e) {
    this.setState({
      shortUrlInput: e.target.value,
      availableResponse: null,
      errorMessage: null,
    });

    this.props.onChange(e.target.value);
  }

  render() {
    const {
      availableResponse,
      errorMessage,
      availableResponseLoading,
    } = this.state;

    // decices whether to render the available short url indicator and with what response
    let availableResponseIndicator = null;
    if (availableResponse !== null) {
      if (availableResponse === true) {
        availableResponseIndicator = <AvailableUrl> Available! </AvailableUrl>;
      } else {
        availableResponseIndicator = <UnavailableUrl> Unavailable :( </UnavailableUrl>;
      }
    }

    return (
      <Container>
        Short URL:

        <ShortUrlInput
          type="text"
          value={this.state.shortUrlInput}
          onChange={this.onChangeShortUrl} />

        <CheckAvailableButton onClick={this.checkIfShortUrlIsAvailable}>
          {availableResponseLoading ? 'Checking...' : 'Available?'}
        </CheckAvailableButton>

        { availableResponseIndicator }

        { errorMessage ? <ErrorMessage> {errorMessage} </ErrorMessage> : null}
      </Container>
    );
  }
}

ShortUrlComponent.propTypes = {
  onChange: PropTypes.func.isRequired,
};

export default ShortUrlComponent;
