// holds validation for the short and long urls

export default class Validations {
  static shortUrlValid(shortUrl) {
    return (shortUrl.match(/^[a-zA-Z0-9]*$/) && (shortUrl.length !== 0));
  }

  static longUrlValid(longUrl) {
    // eslint-disable-next-line no-useless-escape
    return (longUrl.match(/^(?:http(s)?:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/) && (longUrl.length !== 0));
  }
}
