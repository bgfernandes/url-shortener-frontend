import React, { Component } from 'react';
import styled from 'styled-components';

import ShortUrlComponent from './components/ShortUrlComponent';
import RecaptchaComponent from './components/RecaptchaComponent';
import Validations from './Validations';

const Container = styled.div``;
const Paragraph = styled.p``;
const ShortUrlInput = styled.input``;
const GenerateUrlButton = styled.button``;
const ErrorMessage = styled.span`
  color: red;
`;
const StatusMessage = styled.span``;

class App extends Component {
  constructor() {
    super();
    this.state = {
      shortUrl: '',
      longUrl: '',
      errorMessage: null,
      showRecaptcha: false,
      loading: false,
      shortUrlCreationResponse: null,
    };

    this.onGenerateUrlButtonClick = this.onGenerateUrlButtonClick.bind(this);
    this.onChangeShortUrl = this.onChangeShortUrl.bind(this);
    this.onChangeLongUrl = this.onChangeLongUrl.bind(this);
    this.onVerifyCaptcha = this.onVerifyCaptcha.bind(this);
  }

  onGenerateUrlButtonClick() {
    const { shortUrl, loading } = this.state;
    let { longUrl } = this.state;

    // if it is already creating an URL, don't do anything
    if (loading) {
      return;
    }

    // validate shortUrl
    if (!Validations.shortUrlValid(shortUrl)) {
      this.setState({ errorMessage: 'Short URL should have only numbers and letters.' });
      return;
    }

    // if there is no http:// at the start of the long Url, add it
    if ((longUrl.substr(0, 7) !== 'http://') && ((longUrl.substr(0, 8) !== 'https://'))) {
      longUrl = `http://${longUrl}`;
    }

    // validate longUrl
    if (!Validations.longUrlValid(longUrl)) {
      this.setState({ errorMessage: 'The long URL is invalid.' });
      return;
    }

    // show the recaptcha
    this.setState({
      showRecaptcha: true,
      shortUrlCreationResponse: null,
    });
  }

  onChangeShortUrl(shortUrl) {
    this.setState({
      shortUrl,
      errorMessage: null,
      showRecaptcha: false,
      shortUrlCreationResponse: null,
    });
  }

  onChangeLongUrl(longUrl) {
    this.setState({
      longUrl,
      errorMessage: null,
      showRecaptcha: false,
      shortUrlCreationResponse: null,
    });
  }

  onVerifyCaptcha(recaptchaResponse) {
    const { shortUrl } = this.state;
    let { longUrl } = this.state;

    this.setState({
      loading: true,
      showRecaptcha: false,
    });

    // if there is no http:// at the start of the long Url, add it
    if ((longUrl.substr(0, 7) !== 'http://') && ((longUrl.substr(0, 8) !== 'https://'))) {
      longUrl = `http://${longUrl}`;
    }

    const payLoad = {
      shortUrl,
      longUrl,
      recaptchaResponse,
    };

    // note: for simplicity, I intend to host this with the backend,
    // but in my development environment I have the server running in a different port
    let url = '/urls';
    if (process.env.NODE_ENV === 'development') {
      url = `http://localhost:3000${url}`;
    }

    const options = {
      method: 'POST',
      body: JSON.stringify(payLoad),
      headers: { 'Content-Type': 'application/json' },
    };

    fetch(url, options)
      .then(res => res.json())
      .then((res) => {
        if (res.success) {
          this.setState({
            loading: false,
            shortUrlCreationResponse: `Here you go => ${res.finalShortUrl}`,
          });
        } else {
          this.setState({
            loading: false,
            shortUrlCreationResponse: res.message,
          });
        }
      })
      .catch((e) => {
        console.error(e);
        this.setState({
          loading: false,
          shortUrlCreationResponse: 'Something went wrong!',
        });
      });
  }

  render() {
    const {
      errorMessage,
      showRecaptcha,
      loading,
      shortUrlCreationResponse,
    } = this.state;

    let statusMessage = '';
    if (loading) {
      statusMessage = 'Creating the short URL...';
    } else if (shortUrlCreationResponse) {
      statusMessage = shortUrlCreationResponse;
    }

    return (
      <Container>
        <ShortUrlComponent onChange={this.onChangeShortUrl} />

        <Paragraph>
          Long URL:
          <ShortUrlInput
            type="text"
            value={this.state.longUrl}
            onChange={e => this.onChangeLongUrl(e.target.value)} />
        </Paragraph>

        <Paragraph>
          <GenerateUrlButton
            onClick={this.onGenerateUrlButtonClick}>
            Give me that URL!
          </GenerateUrlButton>

          {errorMessage ? <ErrorMessage> {errorMessage} </ErrorMessage> : null}
        </Paragraph>

        {showRecaptcha ? <RecaptchaComponent onVerify={this.onVerifyCaptcha} /> : null}

        <StatusMessage> {statusMessage} </StatusMessage>

      </Container>
    );
  }
}

export default App;
